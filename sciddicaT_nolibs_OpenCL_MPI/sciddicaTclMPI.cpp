#define __CL_ENABLE_EXCEPTIONS
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.hpp>
#include "cl_err_code.h"
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "util.hpp"

#define HEADER_PATH_ID 1
#define DEM_PATH_ID 2
#define SOURCE_PATH_ID 3
#define OUTPUT_PATH_ID 4
#define STEPS_ID 5
#define P_R 0.5
#define P_EPSILON 0.001
#define NUMBER_OF_OUTFLOWS 4
#define STRLEN 256

#define calSetMatrixElement(M, columns, i, j, value) ((M)[(((i) * (columns)) + (j))] = (value))
#define calGetMatrixElement(M, columns, i, j) (M[(((i) * (columns)) + (j))])

void readGISInfo(char* path, int &r, int &c, /*double &xllcorner, double &yllcorner, double &cellsize,*/ double &nodata)
{
  FILE* f;
  
  if ( (f = fopen(path,"r") ) == 0){
    printf("Configuration header file not found\n");
    exit(0);
  }

  char str[STRLEN];
  //int cont = -1;
  //fpos_t position;

  //Reading the header
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); c = atoi(str);         //ncols
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); r = atoi(str);         //nrows
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //xllcorner = atof(str); //xllcorner
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //yllcorner = atof(str); //yllcorner
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //cellsize = atof(str);  //cellsize
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); nodata = atof(str);    //NODATA_value 

//  //Checks if actually there are ncols x nrows values into the file
//  fgetpos (f, &position);
//  while (!feof(f))
//  {
//    fscanf(f,"%s",&str);
//    cont++;
//  }
//  fsetpos (f, &position);
//  if (r * c != cont)
//  {
//    printf("File corrupted\n");
//    exit(0);
//  }
}

void calfLoadMatrix2Dr(double *M, int offset, int rows, int columns, int i_start, int i_end, int j_start, int j_end, FILE *f)
{
  char str[STRLEN];
  int i, j, s;

  for (s = 0; s < offset; s++)
    fscanf(f, "%s", str);

  for (i = 0; i < rows; i++)
    for (j = 0; j < columns; j++)
    {
      fscanf(f, "%s", str);
      calSetMatrixElement(M, columns, i, j, atof(str));
    }
}

bool calLoadMatrix2Dr(double *M, int offset, int rows, int columns, int i_start, int i_end, int j_start, int j_end, char *path)
{
  FILE *f = NULL;
  f = fopen(path, "r");

  if (!f)
    return false;

  calfLoadMatrix2Dr(M, offset, rows, columns, i_start, i_end, j_start, j_end, f);

  fclose(f);

  return true;
}

void calfSaveMatrix2Dr(double *M, int rows, int columns, int i_start, int i_end, int j_start, int j_end, FILE *f)
{
  char str[STRLEN];
  int i, j;

  for (i = i_start; i < i_end; i++)
  {
    for (j = j_start; j < j_end; j++)
    {
      sprintf(str, "%f ", calGetMatrixElement(M, columns, i, j));
      fprintf(f, "%s ", str);
    }
    fprintf(f, "\n");
  }
}

bool calSaveMatrix2Dr(double *M, int rows, int columns, int i_start, int i_end, int j_start, int j_end, char *path, int pid)
{
  FILE *f;
  char path_pid[STRLEN], pid_str[STRLEN];
  sprintf(path_pid, "\0");
  sprintf(pid_str, "%04d", pid);
  strcat(path_pid,path);
  strcat(path_pid,"_pid_");
  strcat(path_pid,pid_str);
  printf("%s\n",path_pid);
  f = fopen(path_pid, "w");
  //f = fopen(path, "w");

  if (!f)
    return false;

  calfSaveMatrix2Dr(M, rows, columns, i_start, i_end, j_start, j_end, f);
  

  fclose(f);

  return true;
}

double *calAddSingleLayerSubstate2Dr(int rows, int columns)
{

  double *tmp = (double *)malloc(sizeof(double) * rows * columns);
  if (!tmp)
    return NULL;
  return tmp;
}

void sciddicaTSimulationInit(int r, int c, double* Sz, double* Sh)
{
  double z, h;
  int i, j;

#pragma omp parallel for
  for (i = 0; i < r; i++)
    for (j = 0; j < c; j++)
    {
      h = calGetMatrixElement(Sh, c, i, j);

      if (h > 0.0)
      {
        z = calGetMatrixElement(Sz, c, i, j);
        calSetMatrixElement(Sz, c, i, j, z - h); 
      }
    }
}

void sendRecvHalos(double* variable, int r, int c, int halo_up_rows, int halo_down_rows)
{
  int pid = -1, np = -1;

  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Status status;

  double* halo_up = variable;
  double* edge_up = variable + halo_up_rows*c;
  double* edge_down = variable + (r-2*halo_down_rows)*c;
  double* halo_down = variable + (r-halo_down_rows)*c;
  int pid_up = (pid > 0) ? (pid - 1) : MPI_PROC_NULL;
  int pid_down = (pid < np - 1) ? (pid + 1) : MPI_PROC_NULL;

  //printf("pid:%d; halo_up:%.0f; edge_up:%.0f; edge_down:%.0f; halo_down:%.0f\n", pid, *halo_up, *edge_up, *edge_down, *halo_down);

  if (pid % 2 == 0) //even
  {
    MPI_Send(edge_up,   halo_up_rows*c,   MPI_DOUBLE, pid_up,   0, MPI_COMM_WORLD);
    MPI_Send(edge_down, halo_down_rows*c, MPI_DOUBLE, pid_down, 0, MPI_COMM_WORLD);
    MPI_Recv(halo_down, halo_down_rows*c, MPI_DOUBLE, pid_down, 0, MPI_COMM_WORLD, &status);
    MPI_Recv(halo_up,   halo_up_rows*c,   MPI_DOUBLE, pid_up,   0, MPI_COMM_WORLD, &status);
  }
  else         //odd
  {
    MPI_Recv(halo_down, halo_down_rows*c, MPI_DOUBLE, pid_down, 0, MPI_COMM_WORLD, &status);
    MPI_Recv(halo_up,   halo_up_rows*c,   MPI_DOUBLE, pid_up,   0, MPI_COMM_WORLD, &status);
    MPI_Send(edge_up,   halo_up_rows*c,   MPI_DOUBLE, pid_up,   0, MPI_COMM_WORLD);
    MPI_Send(edge_down, halo_down_rows*c, MPI_DOUBLE, pid_down, 0, MPI_COMM_WORLD);
  }
}

int main(int argc, char **argv)
{
  int pid = -1, np = -1;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  MPI_Comm_size(MPI_COMM_WORLD, &np);

  //if(np < 2) {
  //  if(0 == pid) printf("Needed 2 or more processes.\n"); MPI_Abort( MPI_COMM_WORLD, 1 ); return 1;
  //}
  
  int rows, cols;
  double  nodata;
  readGISInfo(argv[HEADER_PATH_ID], rows, cols, nodata);

  double *Sz;
  double *Sh;
  double *Sf;
  int r = rows/np; if (pid < rows%np) r++; if (pid == 0 || pid == np-1) r++; else r+=2; //printf("pid: %d, r: %d\n", pid, r);
  int c = cols;
  int i_start = 1, i_end = r-1;
  int j_start = 1, j_end = c-1;
  int Xi[] = {0, -1,  0,  0,  1};
  int Xj[] = {0,  0, -1,  1,  0};
  double p_r = P_R;
  double p_epsilon = P_EPSILON;
  int steps = atoi(argv[STEPS_ID]);
  cl::Buffer d_Sz;
  cl::Buffer d_Sh;
  cl::Buffer d_Sf;
  int halo_rows = 1, 
      halo_up_rows   = pid == 0    ? 0 : halo_rows, 
      halo_down_rows = pid == np-1 ? 0 : halo_rows;
  int edge_up_offset_start   = halo_up_rows*c, 
      edge_down_offset_start = (r-halo_rows-halo_down_rows)*c; 
  int halo_up_offset_start   = 0, 
      halo_down_offset_start = (r-halo_rows)*c; 
  
  Sz = calAddSingleLayerSubstate2Dr(r, c);
  Sh = calAddSingleLayerSubstate2Dr(r, c);
  Sf = calAddSingleLayerSubstate2Dr(NUMBER_OF_OUTFLOWS * r, c);

  int offset_r = 0;
  for (int i=1; i<=pid; i++)
  {
    offset_r += rows/np;
    if (i <= rows%np) 
      offset_r++;
  }
  if (pid > 0)
    offset_r--;

  calLoadMatrix2Dr(Sz, offset_r*c, r, c, i_start, i_end, j_start, j_end, argv[DEM_PATH_ID]);
  calLoadMatrix2Dr(Sh, offset_r*c, r, c, i_start, i_end, j_start, j_end, argv[SOURCE_PATH_ID]);
  MPI_Barrier(MPI_COMM_WORLD);
  sciddicaTSimulationInit(r, c, Sz, Sh);
  MPI_Barrier(MPI_COMM_WORLD);

  try
  {
    // Discover platforms and devices
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    std::string s;
    for (int i = 0; i < platforms.size(); i++)
    {
      platforms[i].getInfo(CL_PLATFORM_NAME, &s);
      if (pid == 0)
        std::cout << "platform[" << i << "]: " << s << std::endl;

      std::vector<cl::Device> devices;
      platforms[i].getDevices(CL_DEVICE_TYPE_ALL, &devices);
      if (pid == 0)
        for (int j = 0; j < devices.size(); j++ )
        {
          devices[j].getInfo(CL_DEVICE_NAME, &s);
          std::cout << "- device[" << j << "]: " << s << std::endl;
        }
    } 

    int platform_id=0;// std::cout << "platform = "; std::cin >> platform_id;
    int device_id=0; //  std::cout << "device   = "; std::cin >> device_id;
    std::vector<cl::Device> devices;
    platforms[platform_id].getDevices(CL_DEVICE_TYPE_ALL, &devices);
    cl::Device device = devices[pid];//devices[device_id];

    // Create a context
    //cl::Context context(DEVICE);
    cl::Context context(device);

    // Load in kernel source, creating a program object for the context
    cl::Program p_reset(context, util::loadProgram("k_reset.cl"), true);
    cl::Program p_computation(context, util::loadProgram("k_computation.cl"), true);
    cl::Program p_balance(context, util::loadProgram("k_balance.cl"), true);

    // // Get the command queue
    cl::CommandQueue queue(context);

    // Create the kernel functors
    auto k_reset = cl::make_kernel<int, int, int, /*cl::Buffer,*/ cl::Buffer>(p_reset, "k_reset");
    auto k_computation = cl::make_kernel<int, int, int, cl::Buffer, cl::Buffer, cl::Buffer, double, double>(p_computation, "k_computation");
    auto k_balance = cl::make_kernel<int, int, int, cl::Buffer, cl::Buffer, cl::Buffer>(p_balance, "k_balance");

    d_Sz = cl::Buffer(context, Sz, Sz+r*c, true);
    d_Sh = cl::Buffer(context, Sh, Sh+r*c, true);
    d_Sf = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(double) * NUMBER_OF_OUTFLOWS*r*c);

    queue.finish();

    int device_vector_width = 64;
    int NDRr = device_vector_width * int( 0.5 + (float)c / device_vector_width );//floor(float(c)/device_vector_width);
    int NDRc = device_vector_width * int( 0.5 + (float)r / device_vector_width );//floor(float(r)/device_vector_width);

    cl::NDRange global(NDRr, NDRc);
    device.getInfo(CL_DEVICE_NAME, &s);
    if (pid == 0) std::cout << "Running simultaion on the " << s << " OpenCL compliant device..." << std::endl;

    MPI_Barrier(MPI_COMM_WORLD);
    util::Timer cl_timer;
    for (int s = 0; s < steps; ++s)
    {
      k_reset(cl::EnqueueArgs(queue,global), r, c, nodata, d_Sf);
      // queue.finish();
      // MPI_Barrier(MPI_COMM_WORLD);
      
      queue.enqueueReadBuffer(d_Sh, true, edge_up_offset_start  *sizeof(double), c*halo_rows*sizeof(double), Sh+edge_up_offset_start  ,NULL,NULL);
      queue.enqueueReadBuffer(d_Sh, true, edge_down_offset_start*sizeof(double), c*halo_rows*sizeof(double), Sh+edge_down_offset_start,NULL,NULL);
      sendRecvHalos(Sh, r, c, halo_up_rows, halo_down_rows);
      queue.enqueueWriteBuffer(d_Sh, true, halo_up_offset_start  *sizeof(double), c*halo_rows*sizeof(double), Sh+halo_up_offset_start  ,NULL,NULL);
      queue.enqueueWriteBuffer(d_Sh, true, halo_down_offset_start*sizeof(double), c*halo_rows*sizeof(double), Sh+halo_down_offset_start,NULL,NULL);
      k_computation(cl::EnqueueArgs(queue,global), r, c, nodata, d_Sz, d_Sh, d_Sf, p_epsilon, p_r);
      queue.finish();
      // MPI_Barrier(MPI_COMM_WORLD);
      
      queue.enqueueReadBuffer(d_Sf, true, (0*r*c+edge_up_offset_start)  *sizeof(double), c*halo_rows*sizeof(double), Sf+0*r*c+edge_up_offset_start  , NULL,NULL);
      queue.enqueueReadBuffer(d_Sf, true, (0*r*c+edge_down_offset_start)*sizeof(double), c*halo_rows*sizeof(double), Sf+0*r*c+edge_down_offset_start, NULL,NULL);
      queue.enqueueReadBuffer(d_Sf, true, (1*r*c+edge_up_offset_start)  *sizeof(double), c*halo_rows*sizeof(double), Sf+1*r*c+edge_up_offset_start  , NULL,NULL);
      queue.enqueueReadBuffer(d_Sf, true, (1*r*c+edge_down_offset_start)*sizeof(double), c*halo_rows*sizeof(double), Sf+1*r*c+edge_down_offset_start, NULL,NULL);
      queue.enqueueReadBuffer(d_Sf, true, (2*r*c+edge_up_offset_start)  *sizeof(double), c*halo_rows*sizeof(double), Sf+2*r*c+edge_up_offset_start  , NULL,NULL);
      queue.enqueueReadBuffer(d_Sf, true, (2*r*c+edge_down_offset_start)*sizeof(double), c*halo_rows*sizeof(double), Sf+2*r*c+edge_down_offset_start, NULL,NULL);
      queue.enqueueReadBuffer(d_Sf, true, (3*r*c+edge_up_offset_start)  *sizeof(double), c*halo_rows*sizeof(double), Sf+3*r*c+edge_up_offset_start  , NULL,NULL);
      queue.enqueueReadBuffer(d_Sf, true, (3*r*c+edge_down_offset_start)*sizeof(double), c*halo_rows*sizeof(double), Sf+3*r*c+edge_down_offset_start, NULL,NULL);
      sendRecvHalos(Sf+0*r*c, r, c, halo_up_rows, halo_down_rows);
      sendRecvHalos(Sf+1*r*c, r, c, halo_up_rows, halo_down_rows);
      sendRecvHalos(Sf+2*r*c, r, c, halo_up_rows, halo_down_rows);
      sendRecvHalos(Sf+3*r*c, r, c, halo_up_rows, halo_down_rows);
      queue.enqueueWriteBuffer(d_Sf, true, (0*r*c+halo_up_offset_start)  *sizeof(double), c*halo_rows*sizeof(double), Sf+0*r*c+halo_up_offset_start  , NULL,NULL);
      queue.enqueueWriteBuffer(d_Sf, true, (0*r*c+halo_down_offset_start)*sizeof(double), c*halo_rows*sizeof(double), Sf+0*r*c+halo_down_offset_start, NULL,NULL);
      queue.enqueueWriteBuffer(d_Sf, true, (1*r*c+halo_up_offset_start)  *sizeof(double), c*halo_rows*sizeof(double), Sf+1*r*c+halo_up_offset_start  , NULL,NULL);
      queue.enqueueWriteBuffer(d_Sf, true, (1*r*c+halo_down_offset_start)*sizeof(double), c*halo_rows*sizeof(double), Sf+1*r*c+halo_down_offset_start, NULL,NULL);
      queue.enqueueWriteBuffer(d_Sf, true, (2*r*c+halo_up_offset_start)  *sizeof(double), c*halo_rows*sizeof(double), Sf+2*r*c+halo_up_offset_start  , NULL,NULL);
      queue.enqueueWriteBuffer(d_Sf, true, (2*r*c+halo_down_offset_start)*sizeof(double), c*halo_rows*sizeof(double), Sf+2*r*c+halo_down_offset_start, NULL,NULL);
      queue.enqueueWriteBuffer(d_Sf, true, (3*r*c+halo_up_offset_start)  *sizeof(double), c*halo_rows*sizeof(double), Sf+3*r*c+halo_up_offset_start  , NULL,NULL);
      queue.enqueueWriteBuffer(d_Sf, true, (3*r*c+halo_down_offset_start)*sizeof(double), c*halo_rows*sizeof(double), Sf+3*r*c+halo_down_offset_start, NULL,NULL);
      k_balance(cl::EnqueueArgs(queue,global), r, c, nodata, d_Sz, d_Sh, d_Sf);
      // queue.finish();
      // MPI_Barrier(MPI_COMM_WORLD);
    }

    if(pid == 0)
    {
      double cl_time = static_cast<double>(cl_timer.getTimeMilliseconds()) / 1000.0;
      printf("Elapsed time: %lf [s]\n", cl_time);
    }

    cl::copy(queue, d_Sh, Sh, Sh+r*c);
    //calSaveMatrix2Dr(Sh, r, c, 0, r, 0, c, argv[OUTPUT_PATH_ID], pid);
    calSaveMatrix2Dr(Sh, r, c, halo_up_rows, r-halo_down_rows, 0, c, argv[OUTPUT_PATH_ID], pid);
  }
  catch (cl::Error err) 
  {
    std::cout << "Exception\n";
    std::cerr 
      << "ERROR: "
      << err.what()
      << "("
      << err_code(err.err())
      << ")"
      << std::endl;
  }

  printf("Releasing memory...\n");
  delete[] Sz;
  delete[] Sh;
  delete[] Sf;

  MPI_Finalize();
  return 0;
}
