#ifndef sciddicaT_KERNEL_H
#define sciddicaT_KERNEL_H

#include "data.h"

void sciddicaTSimulationInit(ACC<double>& Qh, ACC<double>& Qz)
{
  double z, h;

  // printf(" (%i, %f) \n ",OPS_ACC0(0,0), Qh[OPS_ACC0(0,0)]);
  // return;
  //sciddicaT source initialization
  h = Qh(0,0);//calGet2Dr(sciddicaT, Q.h, i, j);

  if ( h > 0.0 ) {
    z = Qz(0,0);// calGet2Dr(sciddicaT, Q.z, i, j);
    Qz(0,0) = z-h; //calSetCurrent2Dr(sciddicaT, Q.z, i, j, z-h);
  }


}


void sciddicaTResetFlows(ACC<double> &Qf0,ACC<double> &Qf1, ACC<double> &Qf2, ACC<double> &Qf3) {
  // printf(" (%i, %f) \n ",OPS_ACC_MD0(0,0,0), Qf[OPS_ACC_MD0(0,0,0)]);
  // printf(" (%i, %f) \n ",OPS_ACC_MD0(1,0,0), Qf[OPS_ACC_MD0(1,0,0)]);
  // printf(" (%i, %f) \n ",OPS_ACC_MD0(2,0,0), Qf[OPS_ACC_MD0(2,0,0)]);
  // printf(" (%i, %f) \n ",OPS_ACC_MD0(3,0,0), Qf[OPS_ACC_MD0(3,0,0)]);
  // printf("  \n ");
  // return;
  Qf0(0,0) =0;
  Qf1(0,0) =0;
  Qf2(0,0) =0;
  Qf3(0,0) =0;
}

void sciddicaTFlowsComputation(ACC<double> &Qf0, ACC<double> &Qf1, ACC<double> &Qf2, ACC<double> &Qf3, ACC<double> &Qh, ACC<double> &Qz) {
 
  char eliminated_cells[5]={0,0,0,0,0};
  char again;
  int cells_count;
  double average;
  double m;
  double u[5];
  int n;
  double z, h;
  int sizeof_X=5;

  m = Qh(0,0) - Pepsilon;

  u[0] = Qz(0,0) + Pepsilon;

  z = Qz(0,-1); //calGetX2Dr(sciddicaT, Q.z, i, j, n);
  h = Qh(0,-1); //calGetX2Dr(sciddicaT, Q.h, i, j, n);
  u[1] = z + h;
  z = Qz(-1,0); //calGetX2Dr(sciddicaT, Q.z, i, j, n);
  h = Qh(-1,0); //calGetX2Dr(sciddicaT, Q.h, i, j, n);
  u[2] = z + h;
  z = Qz(1,0); //calGetX2Dr(sciddicaT, Q.z, i, j, n);
  h = Qh(1,0); //calGetX2Dr(sciddicaT, Q.h, i, j, n);
  u[3] = z + h;
  z = Qz(0,1); //calGetX2Dr(sciddicaT, Q.z, i, j, n);
  h = Qh(0,1); //calGetX2Dr(sciddicaT, Q.h, i, j, n);
  u[4] = z + h;


  //computes outflows
  do{
    again = 0;
    average = m;
    cells_count = 0;

    for (n=0; n<sizeof_X; n++)
      if (!eliminated_cells[n]){
        average += u[n];
        cells_count++;
      }

      if (cells_count != 0)
        average /= cells_count;

      for (n=0; n<sizeof_X; n++)
        if( (average<=u[n]) && (!eliminated_cells[n]) ){
          eliminated_cells[n]=1;
          again=1;
        }
  }while (again);

  // if(m > 0)
  // {
  //   for (n=1; n<sizeof_X; n++)
  //       printf("%f  %f \n",average,  average-u[n]);
  // }

  if (Qh(0,0) <= Pepsilon )
  {
      Qf0(0,0) =0;
      Qf1(0,0) =0;
      Qf2(0,0) =0;
      Qf3(0,0) =0;
  }
  else  
  {  
      if (eliminated_cells[1])
       Qf0(0,0) = 0.0;
      else
       Qf0(0,0) = (average-u[1])*Pr;

      if (eliminated_cells[2])
       Qf1(0,0) = 0.0;
      else
       Qf1(0,0) = (average-u[2])*Pr; 

      if (eliminated_cells[3])
       Qf2(0,0) = 0.0;
      else
       Qf2(0,0) = (average-u[3])*Pr; 

      if (eliminated_cells[4])
       Qf3(0,0) = 0.0;
      else
       Qf3(0,0) = (average-u[4])*Pr; 

  }
}

void sciddicaTWidthUpdate( ACC<double> &Qf0, ACC<double> &Qf1, ACC<double> &Qf2, ACC<double> &Qf3,ACC<double> &Qh) {
  double h_next;


  h_next = Qh(0,0);
  // printf(" Qf3(0,-1) %f \n", Qf3(0,-1));
  // printf(" Qf2(-1,0) %f \n",Qf2(-1,0));
  // printf(" Qf1(1,0) %f \n", Qf1(1,0));
  // printf(" Qf0(0,1) %f \n", Qf0(0,1));
  h_next +=  Qf3(0,-1) - Qf0(0,0);
  h_next +=  Qf2(-1,0) - Qf1(0,0);
  h_next +=  Qf1(1,0)  - Qf2(0,0);
  h_next +=  Qf0(0,1)  - Qf3(0,0);


  Qh(0,0) = h_next; //calSet2Dr(sciddicaT, Q.h, i, j, h_next);

}



#endif //sciddicaT_KERNEL_H
