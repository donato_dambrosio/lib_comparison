
#define calSetMatrixElement(M, xcells, i, j, value) ( (M)[(((i)*(xcells)) + (j))] = (value) )
#define calGetMatrixElement(M, xcells, i, j) ( M[(((i)*(xcells)) + (j))] )
#define STRLEN 256
void calfLoadMatrix2Dr(double* M, int xcells, int ycells, FILE* f, int * d_p, int* d_m)
{
  char str[STRLEN];
  int i, j;
  int cdata=d_p[0],rdata=d_p[1];

  for (i=0; i< ycells ; i++){
  	rdata=d_p[1];
    for (j=0; j< xcells ; j++){
      fscanf(f, "%s", str);
      calSetMatrixElement(M, xcells+(d_p[0]-d_m[0]), cdata, rdata, atof(str));  
      rdata++;
    }
    cdata++;
  }
}

void calfSaveMatrix2Dr(double* M, int xcells, int ycells, FILE* f, int * d_p, int* d_m)
{
  char str[STRLEN];
  int i, j;

  for (i=d_p[0]; i<(ycells+(d_p[0]-d_m[0])+d_m[0]); i++) {
    for (j=d_p[1]; j<(xcells+(d_p[1]-d_m[1])+d_m[1]); j++) {
    	printf("%d\n", (i)*(ycells) + (j));
	    sprintf(str, "%f ", calGetMatrixElement(M, ycells, i, j));
      fprintf(f,"%s ",str);
    }
    fprintf(f,"\n");
  }
}