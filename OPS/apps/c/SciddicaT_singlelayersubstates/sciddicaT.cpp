// standard headers
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <sys/time.h>
#include <stdio.h>
double dx,dy;
#include "util.hpp"

// OPS header file
#define OPS_2D
#include "ops_seq_v2.h"
#include "data.h"
double Pr = 0.5;
double Pepsilon = 0.001;

#include "sciddicaT_kernel.h"
#include "utils.h"

#define DEM_PATH    "../../../../data/tessina_dem.txt"
#define SOURCE_PATH "../../../../data/tessina_source.txt"
#define DEM_PATH_ID 4
#define SOURCE_PATH_ID 5

struct OpenCALTime{

    struct timeval tmStart;

    struct timeval tmEnd;
};

void startTime(struct OpenCALTime * opencalTime){
    gettimeofday(&opencalTime->tmStart, NULL);
}

void endTime(struct OpenCALTime * opencalTime){
    gettimeofday(&opencalTime->tmEnd, NULL);
    unsigned long long seconds =(opencalTime->tmEnd.tv_sec - opencalTime->tmStart.tv_sec) ;
    unsigned long long milliseconds = (opencalTime->tmEnd.tv_usec - opencalTime->tmStart.tv_usec) / 1000;
    unsigned long long totalMilliseconds =1000*seconds + milliseconds;
    int totalSeconds =(int)totalMilliseconds/1000;
    int totalMinutes =(int)totalSeconds/60;
    totalSeconds =(int)totalSeconds%60;
    int totalMilliseconds2 =(int)totalMilliseconds%1000;
    //printf("%d:%d.%d;(min:sec.msec)",totalMinutes,totalSeconds,totalMilliseconds2);
    printf("%llu [ms]\n",totalMilliseconds);
    //printf("%llu\n",seconds);

}


/******************************************************************************
* Main program
*******************************************************************************/
int main(int argc, char **argv)
{
  /**-------------------------- Initialisation --------------------------**/

  int rows   = atoi(argv[1]);
  int cols   = atoi(argv[2]);
  int nsteps = atoi(argv[3]);
  printf("rows  = %d \n", rows);
  printf("cols  = %d \n", cols);
  printf("nsteps = %d \n", nsteps);

  // OPS initialisation
  ops_init(argc,argv,1);

  ops_decl_const("Pr", 1, "double", &Pr);
  ops_decl_const("Pepsilon", 1, "double", &Pepsilon);

  //int rows = 610;// 5;  // real cols
  //int cols = 496;//10; // real rows

  int NUMBER_OF_OUTFLOWS=4;

  //declare block
  ops_block grid2D = ops_decl_block(2, "grid2D");

  int s2D_00_P10_M10_0P1_0M1[]         = {0,0, 1,0, -1,0, 0,1, 0,-1};
  ops_stencil S2D_00_P10_M10_0P1_0M1 = ops_decl_stencil( 2, 5, s2D_00_P10_M10_0P1_0M1, "00:10:-10:01:0-1");

  int s2D_00[]         = {0,0};
  ops_stencil S2D_00 = ops_decl_stencil( 2, 1, s2D_00, "00");
  //const int Pepsilon, const int Pr, double *Qh, double *Qz, double *Qf

  int d_p[2] = {0,0}; //max halo depths for the dat in the positive direction
  int d_m[2] = {0,0}; //max halo depths for the dat in the negative direction
  int size[2] = {cols, rows};
  printf("size[1] %d \n", size[1]);
  printf("size[0] %d \n", size[0]);
  int base[2] = {0,0};

  double* temp = NULL;// = (double*)malloc(sizeof(double)*(rows)*(cols));

  ops_dat Qh    = ops_decl_dat(grid2D, 1, size, base, d_m, d_p, temp, "double", "Qh");
  double *d = (double *) Qh->data;
  FILE * f;
  f  =fopen(argv[SOURCE_PATH_ID],"r");
  calfLoadMatrix2Dr(d, cols, rows, f, d_p, d_m);
  fclose(f);
  Qh->dirty_hd=1;

  double* tempz =NULL;//= (double*)malloc(sizeof(double)*(rows+(d_p[0]-d_m[0]))*(cols+(d_p[0]-d_m[0])));
  ops_dat Qz    = ops_decl_dat(grid2D, 1, size, base, d_m, d_p, tempz, "double", "Qz");
  d = (double *) Qz->data;
  f = fopen(argv[DEM_PATH_ID],"r");
  calfLoadMatrix2Dr(d,cols, rows, f, d_p, d_m);
  fclose(f);
  Qz->dirty_hd=1;
  
  double* tempf = NULL;
  ops_dat Qf0    = ops_decl_dat(grid2D, 1, size, base, d_m, d_p, tempf, "double", "Qf0");
  ops_dat Qf1    = ops_decl_dat(grid2D, 1, size, base, d_m, d_p, tempf, "double", "Qf1");
  ops_dat Qf2    = ops_decl_dat(grid2D, 1, size, base, d_m, d_p, tempf, "double", "Qf2");
  ops_dat Qf3    = ops_decl_dat(grid2D, 1, size, base, d_m, d_p, tempf, "double", "Qf3");

  int iter_range[] = {1,cols-1,1,rows-1};
  ops_par_loop(sciddicaTSimulationInit, "sciddicaTSimulationInit", grid2D, 2, iter_range,
               ops_arg_dat(Qh, 1, S2D_00, "double", OPS_RW),
               ops_arg_dat(Qz, 1, S2D_00, "double", OPS_RW));
/*  f = fopen("h_ops.init","w");
  printf("size[1] %d \n", Qh->size[1]);
  printf("size[0] %d \n", Qh->size[0]);
  for (int i = 1; i < Qh->size[1]-1; i++)
  {
        for (int j = 1; j < Qh->size[0]-1; j++)
        {
            if (fprintf(f, " %3.6lf",((double *)Qh->data)[(i * Qh->size[0] + j) ]) < 0)
            {
              //printf("Error: error writing to %s\n", file_name);
              exit(2);
            }
        }
        fprintf(f, "\n");
  }
  fclose(f);*/
  util::Timer cl_timer;  
  for (int i = 0; i < nsteps; ++i)
  {
      ops_par_loop(sciddicaTResetFlows, "sciddicaTResetFlows", grid2D, 2, iter_range,
                ops_arg_dat(Qf0, 1, S2D_00, "double", OPS_WRITE),
                ops_arg_dat(Qf1, 1, S2D_00, "double", OPS_WRITE),
                ops_arg_dat(Qf2, 1, S2D_00, "double", OPS_WRITE),
                ops_arg_dat(Qf3, 1, S2D_00, "double", OPS_WRITE));

      ops_par_loop(sciddicaTFlowsComputation, "sciddicaTFlowsComputation", grid2D, 2, iter_range,
               ops_arg_dat(Qf0, 1, S2D_00, "double", OPS_RW),
               ops_arg_dat(Qf1, 1, S2D_00, "double", OPS_RW),
               ops_arg_dat(Qf2, 1, S2D_00, "double", OPS_RW),
               ops_arg_dat(Qf3, 1, S2D_00, "double", OPS_RW),
               ops_arg_dat(Qh, 1, S2D_00_P10_M10_0P1_0M1, "double", OPS_RW),
               ops_arg_dat(Qz, 1, S2D_00_P10_M10_0P1_0M1, "double", OPS_RW));


      ops_par_loop(sciddicaTWidthUpdate, "sciddicaTWidthUpdate", grid2D, 2, iter_range,
               ops_arg_dat(Qf0, 1, S2D_00_P10_M10_0P1_0M1, "double", OPS_RW),
               ops_arg_dat(Qf1, 1, S2D_00_P10_M10_0P1_0M1, "double", OPS_RW),
               ops_arg_dat(Qf2, 1, S2D_00_P10_M10_0P1_0M1, "double", OPS_RW),
               ops_arg_dat(Qf3, 1, S2D_00_P10_M10_0P1_0M1, "double", OPS_RW),
               ops_arg_dat(Qh, 1, S2D_00, "double", OPS_RW));

     
  }
  double cl_time = static_cast<double>(cl_timer.getTimeMilliseconds()) / 1000.0;
  printf("Elapsed time: %lf [s]\n", cl_time);

  //ops_print_dat_to_txtfile_core(Qh, "Qh.dat");
  //ops_print_dat_to_txtfile_core(Qz, "z_ops.out");
  //int size2=1;
  //for (int i = 0; i < Qh->block->dims; i++)
   //   size2 *= Qh->size[i];
  //#pragma omp target update from(Qh->data[0 : size2* Qh->elem_size])
  /*f = fopen("h_ops.out","w");
  printf("size[1] %d \n", Qh->size[1]);
  printf("size[0] %d \n", Qh->size[0]);
  for (int i = 0; i < Qh->size[1]; i++)
  {
        for (int j = 0; j < Qh->size[0]; j++)
        {
            if (fprintf(f, "%3.6lf ", ((double *)Qh->data)[(i * Qh->size[0] + j) ]) < 0)
            {
              //printf("Error: error writing to %s\n", file_name);
              exit(2);
            }
        }
        fprintf(f, "\n");
   }
   fclose(f);*/

   ops_exit();
}
