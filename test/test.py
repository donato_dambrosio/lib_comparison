import os
import pprint
import shlex
import subprocess
from datetime import datetime 

class Test:
  folderTest = "" 
  executable = ""
  elapsedTime = ""
  testPassed = ""

  def __init__(self, folderTest, executable):
       self.folderTest = folderTest
       self.executable = executable

############### ENVIRONMENTAL VARIABLES ###################################
pathENVAR = os.environ['PATH']
envFile = "../setenv.txt"
print("Adding the following environmental variables")
#os.system("cat "+ envFile)
command = shlex.split("cat "+envFile)
proc = subprocess.Popen(command, stdout = subprocess.PIPE)
for line in proc.stdout:
  #print(line)
  line = line.replace(b'export ',b'')  
  line = line.replace(b'\n',b'')  
  #print(line)
  if b'PATH=' in line and b'_PATH' not in line: 
      (key, _, value) = line.decode().partition('=')
      os.environ[str(key)] = str(value)+":"+pathENVAR
  else:
      (key, _, value) = line.decode().partition('=')
      os.environ[str(key)] = str(value)
  print(" export "+ str(key)+ " = "+os.environ[str(key)] )
proc.communicate()
############### END ENVIRONMENTAL VARIABLES ####################################

################ PARAMTERS #######################################################################
preString = "../sciddicaT_nolibs_" # pre-string path
numberoftest = 3                   # total number of tests for each executable, min value will use
block_size = [4,8,16,32]
tile_size = [4,8,16,32]
homeTest = os.getcwd()
now = datetime.now()
time_launch_script = now.strftime("%m-%d-%Y_%H-%M-%S")
nameFileResultsGlobal = homeTest+"/resultsGlobal_"+ time_launch_script+".txt"
nameFileResultsLocal  = homeTest+"/resultsLocal_"+ time_launch_script+".txt"
print(nameFileResultsGlobal)
resultsGlobal = {}
resultsLocal  = {}
delimiter = " "
##################################################################################################

tests = []

################################  TEST STANDARD DOMAIN ######################################################################

###### CUDA VERSION############
executable = ["run_straightforward", "run_withouthalocells", "run_withhalocells"]  # target of the Makefile to run the tests
test = Test(preString+"CUDA/", executable)
tests.append(test)

###### OpenCL VERSION############
executable = ["run_straightforward", "run_without_halo_cells"]  # target of the Makefile to run the tests
test = Test(preString+"OpenCL/", executable)
tests.append(test)

###### OPS VERSION############
executable = ["run_ops_cuda", "run_ops_opencl"]  # target of the Makefile to run the tests
test = Test("../OPS/apps/c/SciddicaT_singlelayersubstates/", executable)
tests.append(test)

###### SYCL VERSION############
executable = ["run_straightforward_dpcpp", "run_dpcpp_without_halo_cells"]  # target of the Makefile to run the tests
test = Test(preString+"SYCL/", executable)
tests.append(test)

executable = ["run_straightforward_hipsycl", "run_hipsycl_without_halo_cells"]  # target of the Makefile to run the tests
test = Test(preString+"SYCL/", executable)
tests.append(test)

###############################################################################################################################


################################  TEST EXTENDED DOMAIN ########################################################################
# executable = ["run_straightforward16", "run_withouthalocells16", "run_withhalocells16"]
# t = Test(preString+"CUDA/", executable)
# tests.append(t)

# executable = ["run_straightforward16", "run_without_halo_cells16"]
# t = Test(preString+"OpenCL/", executable)
# tests.append(t)

# executable = ["run_ops_cuda_extented_domain", "run_ops_opencl_extented_domain"]  # target of the Makefile to run the tests
# t = Test("../OPS/apps/c/SciddicaT_singlelayersubstates/", executable)
# tests.append(t)

#executable = ["run_straightforward_dpcpp16", "run_withhout_halo_cells_dpcpp16"]
# executable = ["run_withhout_halo_cells_dpcpp16"]
# t = Test(preString+"SYCL/", executable)
# tests.append(t)

#executable = ["run_straightforward_hipsycl16", "run_withhout_halo_cells_hipsycl16"]
# executable = ["run_withhout_halo_cells_hipsycl16"]
# t = Test(preString+"SYCL/", executable)
# tests.append(t)
###############################################################################################################################

def getElapsedTime(file_name, string_to_search):
   with open(file_name, 'r') as read_obj:
     for line in read_obj:
        if string_to_search in line:
           tmp = line.split(" ")
           return tmp[2]
     return ""

def check_md5sum(file_name, string_to_search):
   """ Check if any line in the file contains given string """
   # Open the file in read only mode
   with open(file_name, 'r') as read_obj:
     # Read all lines in the file one by one
     for line in read_obj:
        # For each line, check if line contains the string
        if string_to_search in line:
           return True
     return False

def runTestGlobal(t, e, make_str, makerunstr, numberoftest, results, key):
    print(make_str)
    os.system(make_str)
    totalElapsedTime = []
    for x in range(0,len(block_size),1):
        for y in range(x,len(block_size),1):
           key2 = str(block_size[x])+"x"+str(block_size[y])
           results[key2] = []
           print("running "+ key2)
           for i in range(0,numberoftest, 1):
              log = "log_"+e
              makerunstr1 = makerunstr + " BLOCK_SIZE_X="+str(block_size[x])+" BLOCK_SIZE_Y="+str(block_size[y])+ " > "+log
              #print(makerunstr1)
              os.system(makerunstr1)
              totalElapsedTime.append(getElapsedTime("./"+log,"Elapsed time:")) 
              if not(check_md5sum("./"+log,"8ed78fa13180c12b4d8aeec7ce6a362a")):
                 print("test not passed")
              results[key2].append(min(totalElapsedTime)) 
           print("min time of "+ str(numberoftest) +" tests :" + str(min(totalElapsedTime)))

           if x != y:
              totalElapsedTime = []
              key2 = str(block_size[y])+"x"+str(block_size[x])
              print("running "+ key2)
              results[key2] = []
              for i in range(0,numberoftest, 1):
                 log = "log_"+e
                 makerunstr1 = makerunstr + " BLOCK_SIZE_X="+str(block_size[y])+" BLOCK_SIZE_Y="+str(block_size[x])+ " > "+log
                 #makerunstr="make "+ e +" > "+log
                 #print(makerunstr1)
                 os.system(makerunstr1+" > "+log)
                 totalElapsedTime.append(getElapsedTime("./"+log,"Elapsed time:")) 
                 if not(check_md5sum("./"+log,"8ed78fa13180c12b4d8aeec7ce6a362a")):
                    print("test not passed")
                 results[key2].append(min(totalElapsedTime)) 
              print("min time of "+ str(numberoftest) +" tests :" + str(min(totalElapsedTime)))

    writeResults(key, results, nameFileResultsGlobal)

def runTestOPSGlobal(t, e, make_str, makerunstr, numberoftest, results, key):
    os.system(make_str)
    print(make_str)
    print(os.getcwd())
    totalElapsedTime = []
    for x in range(0,len(block_size),1):
        for y in range(x,len(block_size),1):
           key2 = str(block_size[x])+"x"+str(block_size[y])
           results[key2] = []
           print("running "+ key2)
           for i in range(0,numberoftest, 1):
             log = "log_"+e
             makerunstr1 = makerunstr + " BLOCK_SIZE_X="+str(block_size[x])+" BLOCK_SIZE_Y="+str(block_size[y])+ " > "+log
             #makerunstr="make "+ e +" > "+log
             #print(makerunstr1)
             os.system(makerunstr1+" > "+log)
             totalElapsedTime.append(getElapsedTime("./"+log,"Elapsed time:")) 
           results[key2].append(min(totalElapsedTime)) 
           print("min time of "+ str(numberoftest) +" tests :" + str(min(totalElapsedTime)))
   
           if x != y:
              totalElapsedTime = []
              key2 = str(block_size[y])+"x"+str(block_size[x])
              results[key2] = []
              print("running "+ key2)
              for i in range(0,numberoftest, 1):
                 log = "log_"+e
                 makerunstr1 = makerunstr + " BLOCK_SIZE_X="+str(block_size[y])+" BLOCK_SIZE_Y="+str(block_size[x])+ " > "+log
                 #makerunstr="make "+ e +" > "+log
                 #print(makerunstr1)
                 os.system(makerunstr1+" > "+log)
                 totalElapsedTime.append(getElapsedTime("./"+log,"Elapsed time:")) 
              results[key2].append(min(totalElapsedTime)) 
              print("min time of "+ str(numberoftest) +" tests :" + str(min(totalElapsedTime)))
   #writeToFile
    writeResults(key, results, nameFileResultsGlobal)


def runTestCUDALocal(t, e, make_str, makerunstr, numberoftest, results, key):
    print(make_str)
    log = "log_"+e
    for t in range(0,len(tile_size),1):
       key2 = str(tile_size[t])
       results[key2] = []
       make_str1 = make_str +" && make TILE_SIZE="+str(tile_size[t])+ " &> logMake"
       #print(make_str1)
       print("running "+str(tile_size[t])+"x"+str(tile_size[t]))
       os.system(make_str1)
       for i in range(0,numberoftest, 1):
          #makerunstr="make "+ e +" > "+log
          #print(makerunstr)
          os.system(makerunstr+" > "+log)
          totalElapsedTime.append(getElapsedTime("./"+log,"Elapsed time:")) 
          if not(check_md5sum("./"+log,"8ed78fa13180c12b4d8aeec7ce6a362a")):
             print("test not passed")
       results[key2].append(min(totalElapsedTime)) 
       print("min time of "+ str(numberoftest) +" tests :" + str(min(totalElapsedTime)))
    writeResults(key, results, nameFileResultsLocal) 


def runTestLocal(t, e, make_str, makerunstr, numberoftest, results, key):
    os.system(make_str)
    print(make_str)
    log = "log_"+e
    for t in range(0,len(tile_size),1):
       print("running "+str(tile_size[t])+"x"+str(tile_size[t]))
       key2 = str(tile_size[t])
       results[key2] = []
       for i in range(0,numberoftest, 1):
          #makerunstr="make "+ e +" > "+log
          os.system(makerunstr+" TILE_SIZE="+str(tile_size[t])+" > "+log)
          #print(makerunstr)
          totalElapsedTime.append(getElapsedTime("./"+log,"Elapsed time:")) 
          if not(check_md5sum("./"+log,"8ed78fa13180c12b4d8aeec7ce6a362a")):
            print("test not passed")
       results[key2].append(min(totalElapsedTime)) 
       print("min time of "+ str(numberoftest) +" tests :" + str(min(totalElapsedTime)))
    writeResults(key, results, nameFileResultsLocal) 


def writeResults(key, l, path):
    print("writing to " + path)
    print(l)
    f = open(path, "a")
    f.write(key)
    for e in l:
        f.write(delimiter+" " + str(min(l[e])))
    f.write("\n")
    f.close()

def addHeaderResultsGlobal(path):
    f = open(path, "w")
    f.write("Name_test ")
    for x in range(0,len(block_size),1):
        for y in range(x,len(block_size),1):
           f.write(delimiter+" "+ str(block_size[x])+"x"+str(block_size[y]))
           if x != y:
              f.write(delimiter+" "+ str(block_size[y])+"x"+str(block_size[x]))
    f.write("\n")
    f.close()

def addHeaderResultsLocal(path):
    f = open(path, "w")
    f.write("Name_test " )
    for t in range(0,len(tile_size),1):
        f.write(delimiter+" "+ str(tile_size[t])+"x"+str(tile_size[t]))
    f.write("\n")
    f.close()


addHeaderResultsGlobal(nameFileResultsGlobal)
addHeaderResultsLocal(nameFileResultsLocal)


for t in tests:
    os.chdir(homeTest)
    os.chdir(t.folderTest)
    print("changedir " + t.folderTest)
    for e in t.executable:
        key = t.folderTest+e
        print("running " + e)
        totalElapsedTime = []
        if "with" in e and "CUDA" in t.folderTest:
            resultsLocal[key] = {}
            make_str = "make clean &> logmakeClean" 
            makerunstr="make "+ e 
            runTestCUDALocal(t, e, make_str, makerunstr, numberoftest, resultsLocal[key], key)
        else:
            if "OPS" in t.folderTest:
                resultsGlobal[key] = {}
                make_str = "make clean &> logmakeClean  && make make_all &> logMake"
                makerunstr = "make "+ e 
                runTestOPSGlobal(t, e, make_str, makerunstr, numberoftest, resultsGlobal[key], key)
            else:
                if "with" in e:
                    resultsLocal[key] = {}
                    make_str = "make clean &> logmakeClean  && make &> logMake"
                    makerunstr = "make "+ e
                    runTestLocal(t, e, make_str, makerunstr, numberoftest, resultsLocal[key], key)
                else:
                    resultsGlobal[key] = {}
                    makestr = "make clean &> logmakeClean && make TILE_SIZE=16 &> logMake"
                    makerunstr = "make "+ e  
                    runTestGlobal(t, e, makestr, makerunstr, numberoftest, resultsGlobal[key], key)

        os.system("rm logmakeClean logMake")           
        #resultsGlobal[key].append(min(totalElapsedTime))        


print(resultsGlobal)
print(resultsLocal)

