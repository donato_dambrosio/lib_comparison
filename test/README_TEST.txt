Guideline to run the tests

The principal script to run the tests is the script python test.py inside the test folder

The test.py script will read the setenv.txt in order to export all the environmental variables in the system.
The first think to do is to set the env variable inside the setenv.txt as follows:

export DPCPP_HOME=/opt/dpc++git
export HIPSYCL_HOME=/opt/hipSYCL/CUDA/bin/
export CUDACC=/opt/cuda/bin/nvcc
export CPPC=g++
export CUDA_ARCH=PASCAL
export PATH=/opt/hipSYCL/CUDA/bin:/opt/dpc++git/bin:$PATH
export LD_LIBRARY_PATH=/opt/dpc++git/lib:$LD_LIBRARY_PATH
export APP=sciddicaT
export OPS_COMPILER=gnu
export MPICXX=mpic++
export NV_ARCH=Pascal
export OPS_INSTALL_PATH=/nfshome/aderango/git/lib_comparison/OPS/ops
export CUDA_INSTALL_PATH=/opt/cuda/
export OPENCL_INSTALL_PATH=/opt/cuda/targets/x86_64-linux/
export OPENCL_INC=/opt/cuda-10.0/include/
export OPENCL_LIB=/opt/cuda-10.0/lib64/
export HDF5_INSTALL_PATH=/nfshome/aderango/wip/hdf5-1.12.0/compiled


the folder structures are configured in order to have in each folder (which identify the different version) a makefile which we use to compile and run the tests.
the structure is the following:

For the standard domain test:

sciddicaT_nolibs_CUDA
    Makefile
        target to compile : make
        target to run     : run_straightforward  BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 
                            run_withouthalocells TILE_SIZE=16
                            run_withhalocells    TILE_SIZE=16

sciddicaT_nolibs_OpenCL
    Makefile
        target to compile : make    
        target to run     : run_straightforward    BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 
                            run_without_halo_cells TILE_SIZE=16

OPS/apps/c/SciddicaT_singlelayersubstates/
    Makefile
        target to compile : make
        target to run     : run_ops_cuda   BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 
                            run_ops_opencl BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 

sciddicaT_nolibs_SYCL
    Makefile
        target to compile : make
        target to run     : run_straightforward_dpcpp      BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16
                            run_dpcpp_without_halo_cells   TILE_SIZE=16                   
                            run_straightforward_hipsycl    BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16
                            run_hipsycl_without_halo_cells TILE_SIZE=16                   


For the extended domain test:

sciddicaT_nolibs_CUDA
    Makefile
        target to compile : make
        target to run     : run_straightforward16   BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 
                            run_withouthalocells16  TILE_SIZE=16
                            run_withhalocells16     TILE_SIZE=16

sciddicaT_nolibs_OpenCL
    Makefile
        target to compile : make
        target to run     : run_straightforward16     BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 
                            run_without_halo_cells16  TILE_SIZE=16

OPS/apps/c/SciddicaT_singlelayersubstates/
    Makefile
        target to compile : make
        target to run     : run_ops_cuda_extented_domain   BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 
                            run_ops_opencl_extented_domain BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 

sciddicaT_nolibs_SYCL
    Makefile
        target to compile : make
        target to run     : run_straightforward_dpcpp16      BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 
                            run_whithout_halo_cells_dpcpp16  TILE_SIZE=16
                            run_straightforward_hipsycl16    BLOCK_SIZE_X=16 BLOCK_SIZE_Y=16 
                            run_hipsycl_without_halo_cells16 TILE_SIZE=16


1) First step  is to make sure that all makefile compile correctly in each folder for the different implementations
2) Second step is to make sure that all makefile target run execute correctly in each folder for the different implementations

Once we have all the environment working we can use the script test.py to run all the tests.
The script test.py will read the file setenv.txt and export all the env var to the script python.
The script test.py uses a list of the Test object to run the tests. A Test object can be defined as follow:

executable = ["run_straightforward", "run_withouthalocells", "run_withhalocells"]  # the Makefile target to run the tests
test = Test(preString+"CUDA/", executable)                                         # the path to the folder and the list of makefile runs
tests.append(test)                                                                 # appending the test Object to the main test list

The script runs all the tests for each element inside the list tests.
Automatically the script will run a "make clean && make " for each test.
Moreover, it will execute tuning runs by changing the BLOCK_SIZE_X and BLOCK_SIZE_Y for the test.

The ouput of the test is written in the folder of the test in two different files: resultsGlobal_dd-mm-yyyy_hh-mm-ss.txt 
                                                                                   resultsLocal_dd-mm-yyyy_hh-mm-ss.txt 

For example:

    resultsGlobal_07-05-2021_09-45-59.txt
    resultsLocal_07-05-2021_09-45-59.txt

Inside the file, you can find all the elasped time for each test.
For example:

Ouput from resultsGlobal_07-04-2021_12-21-38.txt file:

Name test   4x4  4x8  8x4  4x16  16x4  4x32  32x4  8x8  8x16  16x8  8x32  32x8  16x16  16x32  32x16  32x32
../sciddicaT_nolibs_CUDA/run_straightforward  0.771000  0.564000  0.495000  0.495000  0.496000  0.496000  0.508000  0.503000  0.502000  0.496000  0.496000  0.516000  0.501000  0.501000  0.535000  0.535000
../sciddicaT_nolibs_OpenCL/run_straightforward  1.165000  0.819000  0.753000  0.753000  0.698000  0.698000  0.730000  0.721000  0.719000  0.705000  0.705000  0.747000  0.719000  0.719000  0.779000  0.779000
../OPS/apps/c/SciddicaT_singlelayersubstates/run_ops_cuda  0.794000  0.546000  0.524000  0.524000  0.514000  0.514000  0.525000  0.525000  0.525000  0.517000  0.517000  0.539000  0.531000  0.531000  0.547000  0.547000
../OPS/apps/c/SciddicaT_singlelayersubstates/run_ops_opencl  0.957000  0.680000  0.662000  0.662000  0.640000  0.640000  0.649000  0.649000  0.649000  0.648000  0.648000  0.648000  0.648000  0.648000  0.658000  0.658000
../sciddicaT_nolibs_SYCL/run_straightforward_dpcpp  1.216000  0.765000  0.795000  0.758000  0.807000  0.795000  0.818000  0.775000  0.763000  0.769000  0.769000  0.781000  0.773000  0.773000  0.800000  0.800000
../sciddicaT_nolibs_SYCL/run_straightforward_hipsycl  1.604000  1.081000  1.121000  1.091000  1.134000  1.134000  1.097000  1.097000  1.097000  1.069000  1.069000  1.095000  1.095000  1.095000  1.134000  1.134000

Ouput from resultsLocal_07-04-2021_12-21-38.txt file:

Name test                                       4         8        16        32
../sciddicaT_nolibs_CUDA/run_withouthalocells  0.809000  0.483000  0.483000  0.483000
../sciddicaT_nolibs_CUDA/run_withhalocells  2.005000  0.609000  0.498000  0.498000
../sciddicaT_nolibs_OpenCL/run_without_halo_cells  0.999000  0.598000  0.598000  0.598000
../sciddicaT_nolibs_SYCL/run_dpcpp_without_halo_cells  1.237000  0.801000  0.801000  0.801000
../sciddicaT_nolibs_SYCL/run_hipsycl_without_halo_cells  1.371000  0.988000  0.988000  0.988000
