Guideline to run the automatic nvprof profile

The principal script to run the automatic nvprof profile is the python script nvprofAutomatic.py inside the test folder.

The nvprofAutomatic.py script will read the setenv.txt to export all the environmental variables in the system.
The first think to do is to set the env variable inside the setenv.txt as follows:

export DPCPP_HOME=/opt/dpc++git
export HIPSYCL_HOME=/opt/hipSYCL/CUDA/bin/
export CUDACC=/opt/cuda/bin/nvcc
export CPPC=g++
export CUDA_ARCH=PASCAL
export PATH=/opt/hipSYCL/CUDA/bin:/opt/dpc++git/bin:$PATH
export LD_LIBRARY_PATH=/opt/dpc++git/lib:$LD_LIBRARY_PATH
export APP=sciddicaT
export OPS_COMPILER=gnu
export MPICXX=mpic++
export NV_ARCH=Pascal
export OPS_INSTALL_PATH=/nfshome/aderango/git/lib_comparison/OPS/ops
export CUDA_INSTALL_PATH=/opt/cuda/
export OPENCL_INSTALL_PATH=/opt/cuda/targets/x86_64-linux/
export OPENCL_INC=/opt/cuda-10.0/include/
export OPENCL_LIB=/opt/cuda-10.0/lib64/
export HDF5_INSTALL_PATH=/nfshome/aderango/wip/hdf5-1.12.0/compiled


The script will use an object Test to identify the executables for the profile.

For example:

tests = []
executable = ["sciddicaTcudaStraightforward"]                         # the executable to profile
kernelsName = ["sciddicaTWidthUpdate", "sciddicaTFlowsComputation"]   # the kernel name to profile
t2 = Test(preString+"CUDA/", executable, kernelsName)                 # the path to the excutable
tests.append(t2)                                                      # adding to the list tests



The results will write down to a file in the same directory of the script with the name results_nvprof_nameexecutable_dd-mm-yyyy_hh-mm-ss.txt
For example:

Output of the file results_nvprof_sciddicaTcudaStraightforward_07-05-2021_13-13-40.txt

Metrics                   sciddicaTFlowsComputation    sciddicaTWidthUpdate
flop_count_dp             12942411                     2420480
flop_count_sp             860390                       0
flop_count_hp             0                            0
gld_transactions          3015682 2                    723042
gst_transactions          333                          75640
l2_read_transactions      310844                       464978
l2_write_transactions     362                          75669
dram_read_transactions    151446                       381583
dram_write_transactions   42888                        91150
sysmem_read_bytes         32                           32
sysmem_write_bytes        160                          160
shared_load_transactions  0                            0
shared_store_transactions 0                            0
time                      938.57ms                     203.48ms

