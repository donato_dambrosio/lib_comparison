import os
import pprint
import shlex
import subprocess
from datetime import datetime 

class Test:
  folderTest = "" 
  executable = ""
  elapsedTime = ""
  testPassed = ""
  kernelsName = ""

  def __init__(self, folderTest, executable, kernelsName):
       self.folderTest = folderTest
       self.executable = executable
       self.kernelsName = kernelsName

############### ENVIRONMENTAL VARIABLES ###################################
pathENVAR = os.environ['PATH']
envFile = "../setenv.txt"
print("Adding the following environmental variables")
#os.system("cat "+ envFile)
command = shlex.split("cat "+envFile)
proc = subprocess.Popen(command, stdout = subprocess.PIPE)
for line in proc.stdout:
  #print(line)
  line = line.replace(b'export ',b'')  
  line = line.replace(b'\n',b'')  
  #print(line)
  if b'PATH=' in line and b'_PATH' not in line: 
      (key, _, value) = line.decode().partition('=')
      os.environ[str(key)] = str(value)+":"+pathENVAR
  else:
      (key, _, value) = line.decode().partition('=')
      os.environ[str(key)] = str(value)
  print(" export "+ str(key)+ " = "+os.environ[str(key)] )
proc.communicate()
############### ENVIRONMENTAL VARIABLES ####################################

################ PARAMTERS #################################################
preString = "../sciddicaT_nolibs_"
now = datetime.now()
time_launch_script = now.strftime("%m-%d-%Y_%H-%M-%S")
homeTest = os.getcwd()
BLOCK_SIZE_X = 16
BLOCK_SIZE_Y = 16
############################################################################

############### TESTS ######################################################
tests = []
executable = ["sciddicaTcudaStraightforward"]
kernelsName = ["sciddicaTWidthUpdate", "sciddicaTFlowsComputation"]
t2 = Test(preString+"CUDA/", executable, kernelsName)
tests.append(t2)

# executable2 = ["sciddicaTsycl_dpcpp"]
# kernelsName = ["Compute", "Update"]
# t3 = Test(preString+"SYCL/", executable2, kernelsName)
# tests.append(t3)
############################################################################



class nvprofMetric:
  invocation = "" 
  metricName = ""
  metricDescription = ""
  min = ""
  max = ""
  avg = ""

  def __init__(self, 
               invocation, 
               metricName,
               metricDescription,
               min, 
               max, 
               avg):
       self.invocation = invocation
       self.metricName = metricName
       self.metricDescription = metricDescription
       self.min = min
       self.max = max
       self.avg = avg

  def __str__(self):
      strT = self.invocation + " "+ self.metricName + " "+ self.metricDescription+ " "+self.min + " "+ self.max + " "+ self.avg
      return strT


def getMetricsFromCmd(cmd_nvprof):
   output = []
   metric = "--metrics"
   tmp = cmd_nvprof.count(metric)
   for i in range(1,tmp+1,1):
      res = cmd_nvprof.split(metric, maxsplit=i)[-1]\
               .split(maxsplit=i)[0]
      output.append(res)
   return output

def writeResults(results, e, resultsTime):
   firstRow = "Metrics "
   tmpHasp = {}
   for key in results:
      firstRow = firstRow+","+key
   firstCol = []
   firstCol.append("Metrics ")
   numberofmetrics =0
   numberofkernels = len(results)
   for k in results:
      numberofmetrics = len(results[k])
      for m in results[k]:
         firstCol.append(results[k][m].metricName)
      break
   #print("numberofmetrics "+ str(numberofmetrics))
   #print("numberofkernels "+ str(len(results)))
   cols = []
   cols.append(firstCol)
   for k in sorted(results):
      colsN = []
      colsN.append(k)
      for m in results[k]:
         colsN.append(results[k][m].avg)
      cols.append(colsN)
   #print(cols)
   
   f = open(homeTest+"/results_nvprof_"+e+"_"+time_launch_script+".txt", "w")

   for k in range(0,numberofmetrics+1,1):
      for l in range(0, numberofkernels+1,1):
        f.write(" " +cols[l][k])
      f.write("\n")

   f.write(" time ")
   for k in sorted(resultsTime):
      f.write(" "+resultsTime[k])
   f.close()

def getResultsFromLog(e, t, metrics):
      #print(e) 
      file1 = open("./log_nvprof_"+e, 'r')
      count = 0
      results = {}
      while True:
         count += 1
         line = file1.readline()
         if not line:
             break
         for k in t.kernelsName:
            if k in line:
               results[k] = {}
               line = file1.readline()
               for m in metrics:
                  if m in line:
                     line = line.split()
                     tmp = nvprofMetric(
                        line[0],
                        line[1],
                        "",
                        line[len(line)-3],
                        line[len(line)-2],
                        line[len(line)-1],
                     )
                     results[k][m] = tmp
                     line = file1.readline() 
      # print(results) 
      # for k in results:
      #    print(k)
      #    for m in results[k]:
      #       print(m)
      #       print(results[k][m])
      return results

def getResultsFromLogTime(e, t):
      #print(e) 
      file1 = open("./log_nvprof_time"+e, 'r')
      count = 0
      results = {}
      while True:
         count += 1
         line = file1.readline()
         if not line:
             break
         for k in t.kernelsName:
             if k in line:
               line = line.split()
               if "GPU" in line[0]:
                  results[k]= line[3]
               else:
                  results[k]= line[1]
               #print(line)

               
      return results

def runNvprofAndGetOutput(tests, cmd_nvprof, cmd_nvprof_post, cmd_nvprof_time, cmd_nvprof_time_post):
  
   for t in tests:
     os.chdir(t.folderTest)
     print("changedir " + t.folderTest)
     os.system("make clean &> logmakeClean && make &> logMake")
     for e in t.executable:
         cmd_nvprof_tmp      = cmd_nvprof      + " ./"+e+" "+cmd_nvprof_post     +" 2&> ./log_nvprof_"+e
         cmd_nvprof_time_tmp = cmd_nvprof_time + " ./"+e+" "+cmd_nvprof_time_post+" 2&> ./log_nvprof_time"+e
         print(cmd_nvprof_tmp)
         print(cmd_nvprof_time_tmp)
         #exit(0)
         metrics = getMetricsFromCmd(cmd_nvprof)
         os.system(cmd_nvprof_tmp)
         os.system(cmd_nvprof_time_tmp)
         cwd = os.getcwd()
         #print(cwd)
         results = getResultsFromLog(e, t, metrics)
         
         resultsTime = getResultsFromLogTime(e, t)
         print(resultsTime)
         writeResults(results, e, resultsTime)


cmd_nvprof           = "nvprof --metrics flop_count_dp --metrics flop_count_sp --metrics flop_count_hp  --metrics gld_transactions --metrics gst_transactions --metrics l2_read_transactions --metrics l2_write_transactions --metrics dram_read_transactions --metrics dram_write_transactions --metrics sysmem_read_bytes --metrics sysmem_write_bytes --metrics shared_load_transactions --metrics shared_store_transactions"
cmd_nvprof_post      = "../data/tessina_header.txt ../data/tessina_dem.txt ../data/tessina_source.txt ./tessina_output_cuda 10 "+str(BLOCK_SIZE_X)+" "+str(BLOCK_SIZE_Y)
cmd_nvprof_time      = "nvprof --print-gpu-summary"
cmd_nvprof_time_post = "../data/tessina_header.txt ../data/tessina_dem.txt ../data/tessina_source.txt ./tessina_output_cuda 4000 "+str(BLOCK_SIZE_X)+" "+str(BLOCK_SIZE_Y)
runNvprofAndGetOutput(tests,cmd_nvprof, cmd_nvprof_post, cmd_nvprof_time, cmd_nvprof_time_post)


