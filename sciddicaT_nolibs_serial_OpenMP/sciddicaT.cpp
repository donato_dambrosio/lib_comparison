#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "util.hpp"


#define HEADER_PATH_ID 1
#define DEM_PATH_ID 2
#define SOURCE_PATH_ID 3
#define OUTPUT_PATH_ID 4
#define STEPS_ID 5
#define P_R 0.5
#define P_EPSILON 0.001
#define NUMBER_OF_OUTFLOWS 4
#define STRLEN 256

#define calSetMatrixElement(M, columns, i, j, value) (*(M+(((i) * (columns)) + (j))) = (value))
#define calGetMatrixElement(M, columns, i, j) (*(M+(((i) * (columns)) + (j))))
#define calGetBufferedMatrixElement(M, rows, columns, n, i, j) ( *(M+( ((n)*(rows)*(columns)) + ((i)*(columns)) + (j) )) )
#define calSetBufferedMatrixElement(M, rows, columns, n, i, j, value) ( *(M+( ((n)*(rows)*(columns)) + ((i)*(columns)) + (j) )) = (value) )

// #define calSetMatrixElement(M, columns, i, j, value) ((M)[(((i) * (columns)) + (j))] = (value))
// #define calGetMatrixElement(M, columns, i, j) (M[(((i) * (columns)) + (j))])
// #define calGetBufferedMatrixElement(M, rows, columns, n, i, j) ( M[( ((n)*(rows)*(columns)) + ((i)*(columns)) + (j) )] )
// #define calSetBufferedMatrixElement(M, rows, columns, n, i, j, value) ( (M)[( ((n)*(rows)*(columns)) + ((i)*(columns)) + (j) )] = (value) )


void readGISInfo(char* path, int &r, int &c, /*double &xllcorner, double &yllcorner, double &cellsize,*/ double &nodata)
{
  FILE* f;
  
  if ( (f = fopen(path,"r") ) == 0){
    printf("Configuration header file not found\n");
    exit(0);
  }

  char str[STRLEN];
  //int cont = -1;
  //fpos_t position;

  //Reading the header
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); c = atoi(str);         //ncols
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); r = atoi(str);         //nrows
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //xllcorner = atof(str); //xllcorner
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //yllcorner = atof(str); //yllcorner
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); //cellsize = atof(str);  //cellsize
  fscanf(f,"%s",&str); fscanf(f,"%s",&str); nodata = atof(str);    //NODATA_value 

//  //Checks if actually there are ncols x nrows values into the file
//  fgetpos (f, &position);
//  while (!feof(f))
//  {
//    fscanf(f,"%s",&str);
//    cont++;
//  }
//  fsetpos (f, &position);
//  if (r * c != cont)
//  {
//    printf("File corrupted\n");
//    exit(0);
//  }
}

void calfLoadMatrix2Dr(double *M, int rows, int columns, FILE *f)
{
  char str[STRLEN];
  int i, j;

  for (i = 0; i < rows; i++)
    for (j = 0; j < columns; j++)
    {
      fscanf(f, "%s", str);
      calSetMatrixElement(M, columns, i, j, atof(str));
    }
}

bool calLoadMatrix2Dr(double *M, int rows, int columns, char *path)
{
  FILE *f = NULL;
  f = fopen(path, "r");

  if (!f)
    return false;

  calfLoadMatrix2Dr(M, rows, columns, f);

  fclose(f);

  return true;
}

void calfSaveMatrix2Dr(double *M, int rows, int columns, FILE *f)
{
  char str[STRLEN];
  int i, j;

  for (i = 0; i < rows; i++)
  {
    for (j = 0; j < columns; j++)
    {
      sprintf(str, "%f ", calGetMatrixElement(M, columns, i, j));
      fprintf(f, "%s ", str);
    }
    fprintf(f, "\n");
  }
}

bool calSaveMatrix2Dr(double *M, int rows, int columns, char *path)
{
  FILE *f;
  f = fopen(path, "w");

  if (!f)
    return false;

  calfSaveMatrix2Dr(M, rows, columns, f);

  fclose(f);

  return true;
}

double *calAddSingleLayerSubstate2Dr(int rows, int columns)
{

  double *tmp = (double *)malloc(sizeof(double) * rows * columns);
  if (!tmp)
    return NULL;
  return tmp;
}

void sciddicaTResetFlows(int i, int j, int r, int c, double nodata, double* Sf)
{
  calSetBufferedMatrixElement(Sf, r, c, 0, i, j, 0.0);
  calSetBufferedMatrixElement(Sf, r, c, 1, i, j, 0.0);
  calSetBufferedMatrixElement(Sf, r, c, 2, i, j, 0.0);
  calSetBufferedMatrixElement(Sf, r, c, 3, i, j, 0.0);
}

void sciddicaTFlowsComputation(int i, 
int j,
 int r, 
 int c, 
 double nodata, 
 int* __restrict__ Xi, 
 int* __restrict__ Xj, 
 double * __restrict__ Sz, 
 double * __restrict__ Sh, 
 double * __restrict__ Sf, 
 double p_r, 
 double p_epsilon)
{
  bool eliminated_cells[5] = {false, false, false, false, false};
  bool again;
  int cells_count;
  double average;
  double m;
  double u[5];
  int n;
  double z, h;

  m = calGetMatrixElement(Sh, c, i, j) - p_epsilon;
  u[0] = calGetMatrixElement(Sz, c, i, j) + p_epsilon;
  z = calGetMatrixElement(Sz, c, i + Xi[1], j + Xj[1]);
  h = calGetMatrixElement(Sh, c, i + Xi[1], j + Xj[1]);
  u[1] = z + h;                                         
  z = calGetMatrixElement(Sz, c, i + Xi[2], j + Xj[2]);
  h = calGetMatrixElement(Sh, c, i + Xi[2], j + Xj[2]);
  u[2] = z + h;                                         
  z = calGetMatrixElement(Sz, c, i + Xi[3], j + Xj[3]);
  h = calGetMatrixElement(Sh, c, i + Xi[3], j + Xj[3]);
  u[3] = z + h;                                         
  z = calGetMatrixElement(Sz, c, i + Xi[4], j + Xj[4]);
  h = calGetMatrixElement(Sh, c, i + Xi[4], j + Xj[4]);
  u[4] = z + h;

  do
  {
    again = false;
    average = m;
    cells_count = 0;

    for (n = 0; n < 5; n++)
      if (!eliminated_cells[n])
      {
        average += u[n];
        cells_count++;
      }

    if (cells_count != 0)
      average /= cells_count;

    for (n = 0; n < 5; n++)
      if ((average <= u[n]) && (!eliminated_cells[n]))
      {
        eliminated_cells[n] = true;
        again = true;
      }
  } while (again);

  if (!eliminated_cells[1]) calSetBufferedMatrixElement(Sf, r, c, 0, i, j, (average - u[1]) * p_r);
  if (!eliminated_cells[2]) calSetBufferedMatrixElement(Sf, r, c, 1, i, j, (average - u[2]) * p_r);
  if (!eliminated_cells[3]) calSetBufferedMatrixElement(Sf, r, c, 2, i, j, (average - u[3]) * p_r);
  if (!eliminated_cells[4]) calSetBufferedMatrixElement(Sf, r, c, 3, i, j, (average - u[4]) * p_r);
}

void sciddicaTWidthUpdate(int i, int j, int r, int c, double nodata, int* Xi, int* Xj, double *Sz, double *Sh, double *Sf)
{
  double h_next;
  h_next = calGetMatrixElement(Sh, c, i, j);
  h_next += calGetBufferedMatrixElement(Sf, r, c, 3, i+Xi[1], j+Xj[1]) - calGetBufferedMatrixElement(Sf, r, c, 0, i, j);
  h_next += calGetBufferedMatrixElement(Sf, r, c, 2, i+Xi[2], j+Xj[2]) - calGetBufferedMatrixElement(Sf, r, c, 1, i, j);
  h_next += calGetBufferedMatrixElement(Sf, r, c, 1, i+Xi[3], j+Xj[3]) - calGetBufferedMatrixElement(Sf, r, c, 2, i, j);
  h_next += calGetBufferedMatrixElement(Sf, r, c, 0, i+Xi[4], j+Xj[4]) - calGetBufferedMatrixElement(Sf, r, c, 3, i, j);

  calSetMatrixElement(Sh, c, i, j, h_next);
}

void sciddicaTSimulationInit(int r, int c, double* Sz, double* Sh)
{
  double z, h;
  int i, j;

#pragma omp parallel for
  for (i = 0; i < r; i++)
    for (j = 0; j < c; j++)
    {
      h = calGetMatrixElement(Sh, c, i, j);

      if (h > 0.0)
      {
        z = calGetMatrixElement(Sz, c, i, j);
        calSetMatrixElement(Sz, c, i, j, z - h);
      }
    }
}

void f1(int r, int c, double nodata, double* Sf)
{

}

int main(int argc, char **argv)
{
  int rows, cols;
  double  nodata;
  readGISInfo(argv[HEADER_PATH_ID], rows, cols, nodata);

  double *Sz;
  double *Sh;
  double *Sf;
  int r = rows;
  int c = cols;
  int i_start = 1, i_end = r-1;
  int j_start = 1, j_end = c-1;
  int Xi[] = {0, -1,  0,  0,  1};
  int Xj[] = {0,  0, -1,  1,  0};
  double p_r = P_R;
  double p_epsilon = P_EPSILON;
  int steps = atoi(argv[STEPS_ID]);

  Sz = calAddSingleLayerSubstate2Dr(r, c);
  Sh = calAddSingleLayerSubstate2Dr(r, c);
  Sf = calAddSingleLayerSubstate2Dr(NUMBER_OF_OUTFLOWS * r, c);

  calLoadMatrix2Dr(Sz, r, c, argv[DEM_PATH_ID]);
  calLoadMatrix2Dr(Sh, r, c, argv[SOURCE_PATH_ID]);
  sciddicaTSimulationInit(r, c, Sz, Sh);

  util::Timer cl_timer;
  for (int s = 0; s < steps; ++s)
  {
  #pragma omp parallel for
    for (int i = i_start; i < i_end; i++)
      for (int j = j_start; j < j_end; j++)
        sciddicaTResetFlows(i, j, r, c, nodata, Sf);

#pragma omp parallel for
    for (int i = i_start; i < i_end; i++)
    {
    #ifdef __INTEL_COMPILER
    #pragma loop_count(10000)
    #pragma omp simd
    #elif defined(__clang__)
    #pragma clang loop vectorize(assume_safety)
    #elif defined(__GNUC__)
    #pragma GCC ivdep
    #else
    #pragma simd
    
    #endif
      for (int j = j_start; j < j_end; j++)
        //sciddicaTFlowsComputation(i, j, r, c, nodata, Xi, Xj, Sz, Sh, Sf, p_r, p_epsilon);
        {
            bool eliminated_cells[5] = {false, false, false, false, false};
  bool again;
  int cells_count;
  double average;
  double m;
  double u[5];
  int n;
  double z, h;

  m = calGetMatrixElement(Sh, c, i, j) - p_epsilon;
  u[0] = calGetMatrixElement(Sz, c, i, j) + p_epsilon;
  z = calGetMatrixElement(Sz, c, i + -1, j + 0);
  h = calGetMatrixElement(Sh, c, i + -1, j + 0);
  u[1] = z + h;                                         
  z = calGetMatrixElement(Sz, c, i + 0, j + -1);
  h = calGetMatrixElement(Sh, c, i + 0, j + -1);
  u[2] = z + h;                                         
  z = calGetMatrixElement(Sz, c, i + 0, j + 1);
  h = calGetMatrixElement(Sh, c, i + 0, j + 1);
  u[3] = z + h;                                         
  z = calGetMatrixElement(Sz, c, i + 1, j + 0);
  h = calGetMatrixElement(Sh, c, i + 1, j + 0);
  u[4] = z + h;
  do
  {
    again = false;
    average = m;
    cells_count = 0;

    for (n = 0; n < 5; n++)
      if (!eliminated_cells[n])
      {
        average += u[n];
        cells_count++;
      }

    if (cells_count != 0)
      average /= cells_count;

    for (n = 0; n < 5; n++)
      if ((average <= u[n]) && (!eliminated_cells[n]))
      {
        eliminated_cells[n] = true;
        again = true;
      }
  } while (again);

  if (!eliminated_cells[1]) calSetBufferedMatrixElement(Sf, r, c, 0, i, j, (average - u[1]) * p_r);
  if (!eliminated_cells[2]) calSetBufferedMatrixElement(Sf, r, c, 1, i, j, (average - u[2]) * p_r);
  if (!eliminated_cells[3]) calSetBufferedMatrixElement(Sf, r, c, 2, i, j, (average - u[3]) * p_r);
  if (!eliminated_cells[4]) calSetBufferedMatrixElement(Sf, r, c, 3, i, j, (average - u[4]) * p_r);
        }
    }

#pragma omp parallel for
    for (int i = i_start; i < i_end; i++)
      for (int j = j_start; j < j_end; j++)
        sciddicaTWidthUpdate(i, j, r, c, nodata, Xi, Xj, Sz, Sh, Sf);
  }
  double cl_time = static_cast<double>(cl_timer.getTimeMilliseconds()) / 1000.0;
  printf("Elapsed time: %lf [s]\n", cl_time);

  calSaveMatrix2Dr(Sh, r, c, argv[OUTPUT_PATH_ID]);

  printf("Releasing memory...\n");
  delete[] Sz;
  delete[] Sh;
  delete[] Sf;

  return 0;
}
